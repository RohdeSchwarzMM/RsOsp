Rohde & Schwarz OSP Opens Switch Platform RsOsp instrument driver.

Supported instruments: OSP

The package is hosted here: https://pypi.org/project/RsOsp/

Documentation: https://RsOsp.readthedocs.io/

Examples: https://github.com/Rohde-Schwarz/Examples/tree/main/Misc/Python/RsOsp_ScpiPackage

----------------------------------------------------------------------------------

Release Notes:

Latest release notes summary: Release for new OSP FW 2.10.17

Version 2.10.17.75

- Release for new OSP FW 2.10.17

Version 1.0.4.57

- Added documentation on ReadTheDocs

Version 1.0.4.57

- Fixed formatting of string in methods with list of paths as inputs

Version 1.0.3.55

- Changed responses for methods with List[string] return values: If the instrument returns exactly one empty string, the methods return empty List []

Version 1.0.2.54

- Fixed parsing of the instrument errors when an error message contains two double quotes

Version 1.0.0.50

- First released version