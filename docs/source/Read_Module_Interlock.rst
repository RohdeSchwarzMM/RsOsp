Interlock
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:MODule:INTerlock

.. code-block:: python

	READ:MODule:INTerlock



.. autoclass:: RsOsp.Implementations.Read_.Module_.Interlock.Interlock
	:members:
	:undoc-members:
	:noindex: