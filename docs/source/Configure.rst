Configure
----------------------------------------





.. autoclass:: RsOsp.Implementations.Configure.Configure
	:members:
	:undoc-members:
	:noindex:



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Frame.rst
	Configure_Compatible.rst
	Configure_Virtual.rst
	Configure_MainInfo.rst
	Configure_PowerUp.rst
	Configure_All.rst
	Configure_Relay.rst
	Configure_Lock.rst