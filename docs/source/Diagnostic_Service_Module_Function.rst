Function
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: DIAGnostic:SERVice:MODule:FUNCtion

.. code-block:: python

	DIAGnostic:SERVice:MODule:FUNCtion



.. autoclass:: RsOsp.Implementations.Diagnostic_.Service_.Module_.Function.Function
	:members:
	:undoc-members:
	:noindex: