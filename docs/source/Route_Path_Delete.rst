Delete
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: ROUTe:PATH:DELete:NAME

.. code-block:: python

	ROUTe:PATH:DELete:NAME



.. autoclass:: RsOsp.Implementations.Route_.Path_.Delete.Delete
	:members:
	:undoc-members:
	:noindex: