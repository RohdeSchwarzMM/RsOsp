Route
----------------------------------------





.. autoclass:: RsOsp.Implementations.Route.Route
	:members:
	:undoc-members:
	:noindex:



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Route_Close.rst
	Route_Attenuation.rst
	Route_Phase.rst
	Route_Path.rst