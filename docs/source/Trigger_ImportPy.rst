ImportPy
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: TRIGger:IMPort:DELete
	single: TRIGger:IMPort:DELete:ALL
	single: TRIGger:IMPort

.. code-block:: python

	TRIGger:IMPort:DELete
	TRIGger:IMPort:DELete:ALL
	TRIGger:IMPort



.. autoclass:: RsOsp.Implementations.Trigger_.ImportPy.ImportPy
	:members:
	:undoc-members:
	:noindex:



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_ImportPy_Catalog.rst