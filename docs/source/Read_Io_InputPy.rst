InputPy
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:IO:IN

.. code-block:: python

	READ:IO:IN



.. autoclass:: RsOsp.Implementations.Read_.Io_.InputPy.InputPy
	:members:
	:undoc-members:
	:noindex: