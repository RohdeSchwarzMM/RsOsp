Attenuation
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: ROUTe:ATTenuation

.. code-block:: python

	ROUTe:ATTenuation



.. autoclass:: RsOsp.Implementations.Route_.Attenuation.Attenuation
	:members:
	:undoc-members:
	:noindex: