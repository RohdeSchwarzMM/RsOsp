ImportPy
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:FRAMe:IMPort:DELete
	single: CONFigure:FRAMe:IMPort:DELete:ALL
	single: CONFigure:FRAMe:IMPort

.. code-block:: python

	CONFigure:FRAMe:IMPort:DELete
	CONFigure:FRAMe:IMPort:DELete:ALL
	CONFigure:FRAMe:IMPort



.. autoclass:: RsOsp.Implementations.Configure_.Frame_.ImportPy.ImportPy
	:members:
	:undoc-members:
	:noindex:



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Frame_ImportPy_Catalog.rst