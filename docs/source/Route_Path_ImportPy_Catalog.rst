Catalog
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: ROUTe:PATH:IMPort:CATalog

.. code-block:: python

	ROUTe:PATH:IMPort:CATalog



.. autoclass:: RsOsp.Implementations.Route_.Path_.ImportPy_.Catalog.Catalog
	:members:
	:undoc-members:
	:noindex: