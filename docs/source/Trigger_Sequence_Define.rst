Define
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: TRIGger:SEQuence:DEFine:ALL
	single: TRIGger:SEQuence:DEFine:LENGth

.. code-block:: python

	TRIGger:SEQuence:DEFine:ALL
	TRIGger:SEQuence:DEFine:LENGth



.. autoclass:: RsOsp.Implementations.Trigger_.Sequence_.Define.Define
	:members:
	:undoc-members:
	:noindex:



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_Sequence_Define_Entry.rst