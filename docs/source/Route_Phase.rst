Phase
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: ROUTe:PHASe

.. code-block:: python

	ROUTe:PHASe



.. autoclass:: RsOsp.Implementations.Route_.Phase.Phase
	:members:
	:undoc-members:
	:noindex: