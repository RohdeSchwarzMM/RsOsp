HwInfo
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: DIAGnostic:SERVice:MODule:HWINfo

.. code-block:: python

	DIAGnostic:SERVice:MODule:HWINfo



.. autoclass:: RsOsp.Implementations.Diagnostic_.Service_.Module_.HwInfo.HwInfo
	:members:
	:undoc-members:
	:noindex: