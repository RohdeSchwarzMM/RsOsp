MainInfo
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:MAINinfo:TEXT
	single: CONFigure:MAINinfo:PATH

.. code-block:: python

	CONFigure:MAINinfo:TEXT
	CONFigure:MAINinfo:PATH



.. autoclass:: RsOsp.Implementations.Configure_.MainInfo.MainInfo
	:members:
	:undoc-members:
	:noindex: