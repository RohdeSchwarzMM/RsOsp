Trigger
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: TRIGger:STATe
	single: TRIGger:TYPE
	single: TRIGger:INDex
	single: TRIGger:EXPort

.. code-block:: python

	TRIGger:STATe
	TRIGger:TYPE
	TRIGger:INDex
	TRIGger:EXPort



.. autoclass:: RsOsp.Implementations.Trigger.Trigger
	:members:
	:undoc-members:
	:noindex:



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_Signal.rst
	Trigger_Sequence.rst
	Trigger_Execute.rst
	Trigger_Count.rst
	Trigger_ImportPy.rst