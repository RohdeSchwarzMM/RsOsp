Entry
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: TRIGger:SEQuence:DEFine:ENTRy

.. code-block:: python

	TRIGger:SEQuence:DEFine:ENTRy



.. autoclass:: RsOsp.Implementations.Trigger_.Sequence_.Define_.Entry.Entry
	:members:
	:undoc-members:
	:noindex: