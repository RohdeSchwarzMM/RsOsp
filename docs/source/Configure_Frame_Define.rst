Define
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:FRAMe:DEFine

.. code-block:: python

	CONFigure:FRAMe:DEFine



.. autoclass:: RsOsp.Implementations.Configure_.Frame_.Define.Define
	:members:
	:undoc-members:
	:noindex: