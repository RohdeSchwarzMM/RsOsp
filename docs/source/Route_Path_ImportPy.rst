ImportPy
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: ROUTe:PATH:IMPort:DELete
	single: ROUTe:PATH:IMPort:DELete:ALL
	single: ROUTe:PATH:IMPort

.. code-block:: python

	ROUTe:PATH:IMPort:DELete
	ROUTe:PATH:IMPort:DELete:ALL
	ROUTe:PATH:IMPort



.. autoclass:: RsOsp.Implementations.Route_.Path_.ImportPy.ImportPy
	:members:
	:undoc-members:
	:noindex:



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Route_Path_ImportPy_Catalog.rst