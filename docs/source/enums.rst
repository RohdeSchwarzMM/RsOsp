Enums
=========

ReplaceOrKeep
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ReplaceOrKeep.KEEP
	# All values (2x):
	KEEP | REPLace

TriggerExecType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TriggerExecType.RESet
	# All values (2x):
	RESet | TRIGger

TriggerSlope
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TriggerSlope.BOTH
	# All values (3x):
	BOTH | NEGative | POSitive

TriggerType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TriggerType.ADDRessed
	# All values (4x):
	ADDRessed | SEQuenced | SINGle | TOGGle

