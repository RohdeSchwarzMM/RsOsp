Insert
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:FRAMe:INSert

.. code-block:: python

	CONFigure:FRAMe:INSert



.. autoclass:: RsOsp.Implementations.Configure_.Frame_.Insert.Insert
	:members:
	:undoc-members:
	:noindex: