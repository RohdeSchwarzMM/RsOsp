Close
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: ROUTe:CLOSe

.. code-block:: python

	ROUTe:CLOSe



.. autoclass:: RsOsp.Implementations.Route_.Close.Close
	:members:
	:undoc-members:
	:noindex: