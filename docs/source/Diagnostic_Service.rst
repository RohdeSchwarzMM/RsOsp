Service
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: DIAGnostic:SERVice:HWINfo

.. code-block:: python

	DIAGnostic:SERVice:HWINfo



.. autoclass:: RsOsp.Implementations.Diagnostic_.Service.Service
	:members:
	:undoc-members:
	:noindex:



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Diagnostic_Service_Module.rst
	Diagnostic_Service_User.rst