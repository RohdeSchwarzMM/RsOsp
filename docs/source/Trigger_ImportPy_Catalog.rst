Catalog
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: TRIGger:IMPort:CATalog

.. code-block:: python

	TRIGger:IMPort:CATalog



.. autoclass:: RsOsp.Implementations.Trigger_.ImportPy_.Catalog.Catalog
	:members:
	:undoc-members:
	:noindex: