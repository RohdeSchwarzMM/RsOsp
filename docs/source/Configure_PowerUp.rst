PowerUp
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:POWerup:PATH
	single: CONFigure:POWerup:RESet

.. code-block:: python

	CONFigure:POWerup:PATH
	CONFigure:POWerup:RESet



.. autoclass:: RsOsp.Implementations.Configure_.PowerUp.PowerUp
	:members:
	:undoc-members:
	:noindex: