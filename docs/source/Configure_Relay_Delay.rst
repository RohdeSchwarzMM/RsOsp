Delay
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:RELay:DELay

.. code-block:: python

	CONFigure:RELay:DELay



.. autoclass:: RsOsp.Implementations.Configure_.Relay_.Delay.Delay
	:members:
	:undoc-members:
	:noindex: