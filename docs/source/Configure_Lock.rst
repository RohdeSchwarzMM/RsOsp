Lock
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:LOCK:MODE

.. code-block:: python

	CONFigure:LOCK:MODE



.. autoclass:: RsOsp.Implementations.Configure_.Lock.Lock
	:members:
	:undoc-members:
	:noindex: