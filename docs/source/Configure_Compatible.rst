Compatible
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:COMPatible:MODE

.. code-block:: python

	CONFigure:COMPatible:MODE



.. autoclass:: RsOsp.Implementations.Configure_.Compatible.Compatible
	:members:
	:undoc-members:
	:noindex: