Count
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: TRIGger:COUNt:VALue
	single: TRIGger:COUNt:OVERflow

.. code-block:: python

	TRIGger:COUNt:VALue
	TRIGger:COUNt:OVERflow



.. autoclass:: RsOsp.Implementations.Trigger_.Count.Count
	:members:
	:undoc-members:
	:noindex: