Frame
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:FRAMe:CATalog
	single: CONFigure:FRAMe:ADD
	single: CONFigure:FRAMe:DELete
	single: CONFigure:FRAMe:DELete:ALL
	single: CONFigure:FRAMe:EXPort

.. code-block:: python

	CONFigure:FRAMe:CATalog
	CONFigure:FRAMe:ADD
	CONFigure:FRAMe:DELete
	CONFigure:FRAMe:DELete:ALL
	CONFigure:FRAMe:EXPort



.. autoclass:: RsOsp.Implementations.Configure_.Frame.Frame
	:members:
	:undoc-members:
	:noindex:



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Frame_Define.rst
	Configure_Frame_Insert.rst
	Configure_Frame_ImportPy.rst