Execute
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: TRIGger:EXECute

.. code-block:: python

	TRIGger:EXECute



.. autoclass:: RsOsp.Implementations.Trigger_.Execute.Execute
	:members:
	:undoc-members:
	:noindex: