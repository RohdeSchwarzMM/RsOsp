All
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:ALL:BACKup

.. code-block:: python

	CONFigure:ALL:BACKup



.. autoclass:: RsOsp.Implementations.Configure_.All.All
	:members:
	:undoc-members:
	:noindex:



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_All_Restore.rst