Define
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: ROUTe:PATH:DEFine

.. code-block:: python

	ROUTe:PATH:DEFine



.. autoclass:: RsOsp.Implementations.Route_.Path_.Define.Define
	:members:
	:undoc-members:
	:noindex: