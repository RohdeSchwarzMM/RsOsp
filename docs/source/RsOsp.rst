RsOsp API Structure
========================================


.. autoclass:: RsOsp.RsOsp
	:members:
	:undoc-members:
	:noindex:

.. rubric:: Subgroups

.. toctree::
	:maxdepth: 6
	:glob:

	Configure.rst
	Read.rst
	Route.rst
	Diagnostic.rst
	Trigger.rst