Path
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: ROUTe:PATH:CATalog
	single: ROUTe:PATH:LAST
	single: ROUTe:PATH:DELete:ALL
	single: ROUTe:PATH:EXPort

.. code-block:: python

	ROUTe:PATH:CATalog
	ROUTe:PATH:LAST
	ROUTe:PATH:DELete:ALL
	ROUTe:PATH:EXPort



.. autoclass:: RsOsp.Implementations.Route_.Path.Path
	:members:
	:undoc-members:
	:noindex:



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Route_Path_Define.rst
	Route_Path_Delete.rst
	Route_Path_ImportPy.rst