Examples
======================

For more examples, visit our `Rohde & Schwarz Github repository <https://github.com/Rohde-Schwarz/Examples/tree/main/Misc/Python/RsOsp_ScpiPackage>`_.



.. literalinclude:: RsOsp_GettingStarted_Example.py



.. literalinclude:: RsOsp_Example.py

