Signal
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: TRIGger:SIGNal:LEVel
	single: TRIGger:SIGNal:SLOPe
	single: TRIGger:SIGNal:TERMination

.. code-block:: python

	TRIGger:SIGNal:LEVel
	TRIGger:SIGNal:SLOPe
	TRIGger:SIGNal:TERMination



.. autoclass:: RsOsp.Implementations.Trigger_.Signal.Signal
	:members:
	:undoc-members:
	:noindex: