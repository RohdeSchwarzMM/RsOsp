Temperature
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: DIAGnostic:SERVice:MODule:TEMPerature

.. code-block:: python

	DIAGnostic:SERVice:MODule:TEMPerature



.. autoclass:: RsOsp.Implementations.Diagnostic_.Service_.Module_.Temperature.Temperature
	:members:
	:undoc-members:
	:noindex: