Virtual
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:VIRTual:MODE

.. code-block:: python

	CONFigure:VIRTual:MODE



.. autoclass:: RsOsp.Implementations.Configure_.Virtual.Virtual
	:members:
	:undoc-members:
	:noindex: