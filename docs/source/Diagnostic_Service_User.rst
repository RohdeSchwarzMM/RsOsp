User
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: DIAGnostic:SERVice:USER:ERRor
	single: DIAGnostic:SERVice:USER:WARNing

.. code-block:: python

	DIAGnostic:SERVice:USER:ERRor
	DIAGnostic:SERVice:USER:WARNing



.. autoclass:: RsOsp.Implementations.Diagnostic_.Service_.User.User
	:members:
	:undoc-members:
	:noindex: