Operations
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:RELay:OPERations

.. code-block:: python

	READ:RELay:OPERations



.. autoclass:: RsOsp.Implementations.Read_.Relay_.Operations.Operations
	:members:
	:undoc-members:
	:noindex: