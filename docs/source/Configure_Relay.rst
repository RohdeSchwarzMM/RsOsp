Relay
----------------------------------------





.. autoclass:: RsOsp.Implementations.Configure_.Relay.Relay
	:members:
	:undoc-members:
	:noindex:



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Relay_Delay.rst